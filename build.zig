// SPDX-FileCopyrightText: Yorhel <projects@yorhel.nl>
// SPDX-License-Identifier: MIT

const std = @import("std");

const version = "0.3";

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{
        .preferred_optimize_mode = .ReleaseFast,
    });

    const exe = b.addExecutable(.{
        .name = "torreas",
        .root_source_file = b.path("main.zig"),
        .target = target,
        .optimize = optimize,
        .single_threaded = true,
    });
    b.installArtifact(exe);

    const exe_options = b.addOptions();
    exe.root_module.addOptions("build_options", exe_options);
    exe_options.addOption([]const u8, "version", version);

    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| run_cmd.addArgs(args);
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const unit_tests = b.addTest(.{
        .root_source_file = b.path("main.zig"),
        .target = target,
        .optimize = optimize,
    });
    const run_unit_tests = b.addRunArtifact(unit_tests);
    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);

    b.installFile("torreas.1", "share/man/man1/torreas.1");

    // This is awful \o/
    // I'll be the only one using this, so w/e.
    const dist_cmd = b.addSystemCommand(&[_][]const u8{
        "sh", "-c",
        std.fmt.comptimePrint(
            \\rm -f torreas-{0s}.tar.gz
            \\mkdir torreas-{0s}
            \\for f in `git ls-files | grep -v ^\\.gitignore`; do
            \\  mkdir -p torreas-{0s}/`dirname $f`
            \\  ln -s "`pwd`/$f" torreas-{0s}/$f
            \\done
            \\tar -cophzf torreas-{0s}.tar.gz --sort=name torreas-{0s}
            \\rm -rf torreas-{0s}
        , .{version}),
    });
    const dist_step = b.step("dist", "Create a release tarball");
    dist_step.dependOn(&dist_cmd.step);
}
