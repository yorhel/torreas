// SPDX-FileCopyrightText: Yorhel <projects@yorhel.nl>
// SPDX-License-Identifier: MIT

const std = @import("std");

const global = struct {
    var alloc_state = std.heap.GeneralPurposeAllocator(.{}){};
    const alloc = alloc_state.allocator();

    var progress = std.Progress{};
    var log_level = std.log.Level.info;
    var partial = false;
    var torrent: Torrent = undefined;
    var paths: []Path = undefined;
    var candidates: []?*FSFile = undefined; // indices correspond to torrent.files
    var pieces: []Piece = undefined; // correspond to torrent.pieces
};

pub const std_options = .{
    .logFn = myLogFn,
    .log_level = .debug,
};


pub fn myLogFn(comptime level: std.log.Level, comptime _: @TypeOf(.EnumLiteral), comptime fmt: []const u8, args: anytype) void {
    if (@intFromEnum(level) > @intFromEnum(global.log_level)) return;
    // Should be progress.log(), but that doesn't clear progress information and thus creates a bit of a mess.
    global.progress.lock_stderr();
    const stderr = std.io.getStdErr().writer();
    var buf = std.io.bufferedWriter(stderr);
    buf.writer().print(fmt ++ "\n", args) catch {};
    buf.flush() catch {};
    global.progress.unlock_stderr();
}


fn usage(help: bool) noreturn {
    const ow = if (help) std.io.getStdOut().writer() else std.io.getStdErr().writer();
    ow.writeAll(
        \\torreas [options] torrent-file dirs
        \\
        \\  --help         This help text
        \\  --version      Show version
        \\  --verbose      More verbose output
        \\  --silent       Suppress progress output
        \\  --partial      Continue even if some files are missing
        \\
        \\  --copy-sh      Output shell script to copy files
        \\  --symlink-sh   Output shell script to symlink files
        \\  --hardlink-sh  Output shell script to hard link files
        \\
    ) catch {};
    std.posix.exit(if (help) 0 else 1);
}


fn die(comptime fmt: []const u8, args: anytype) noreturn {
    std.log.err(fmt, args);
    std.process.exit(1);
}

fn unableToComplete() void {
    if (!global.partial)
        die("Unable to assemble all files, re-run with --partial to attempt an incomplete assembly.", .{});
}


// Crappy bencode decoder
const BenDecoder = struct {
    buf: []const u8,

    const D = *@This();

    fn digit(c: u8) !u4 {
        return if (c < '0' or c > '9') error.InvalidInteger else @intCast(c - '0');
    }

    fn peek(d: D) !u8 {
        if (d.buf.len == 0) return error.UnexpectedEndOfFile;
        return d.buf[0];
    }

    fn con(d: D) !u8 {
        defer d.buf = d.buf[1..];
        return d.peek();
    }

    fn num(d: D, comptime T: type, eon: u8) !T {
        const neg = try d.peek() == '-';
        if (neg) _ = try d.con();
        var v: T = try digit(try d.con());
        if (v == 0) {
            if (neg) return error.InvalidInteger;
            if (try d.con() != eon) return error.InvalidInteger;
            return 0;
        }
        while (true) {
            const c = try d.con();
            if (c == eon) break;
            v = try std.math.add(T, try digit(c), try std.math.mul(T, v, 10));
        }
        return if (neg) try std.math.negate(v) else v;
    }

    fn int(d: D, comptime T: type) !T {
        if (try d.con() != 'i') return error.ExpectedInteger;
        return d.num(T, 'e');
    }

    fn str(d: D) ![]const u8 {
        const len = try d.num(usize, ':');
        if (d.buf.len < len) return error.InvalidString;
        defer d.buf = d.buf[len..];
        return d.buf[0..len];
    }

    fn list(d: D) !void {
        if (try d.con() != 'l') return error.ExpectedList;
    }

    fn dict(d: D) !void {
        if (try d.con() != 'd') return error.ExpectedDict;
    }

    fn end(d: D) !bool {
        if (try d.peek() == 'e') {
            _ = try d.con();
            return true;
        }
        return false;
    }

    fn skip(d: D) !void {
        switch (try d.peek()) {
            'i' => _ = try d.int(i64),
            'l','d' => {
                _ = try d.con();
                while (!try d.end()) try d.skip();
            },
            else => _ = try d.str(),
        }
    }
};


test "bencode decoder" {
    var b = BenDecoder{.buf = "12:Hello, world"};
    try std.testing.expectEqualStrings(try b.str(), "Hello, world");

    b.buf = "13:Hello, world";
    try std.testing.expectError(error.InvalidString, b.str());

    b.buf = "0:";
    try std.testing.expectEqualStrings(try b.str(), "");

    b.buf = "-1:";
    try std.testing.expectError(error.Overflow, b.str());

    b.buf = "i124e";
    try std.testing.expectEqual(try b.int(u7), 124);

    b.buf = "i-124e";
    try std.testing.expectEqual(try b.int(i8), -124);

    b.buf = "i128e";
    try std.testing.expectError(error.Overflow, b.int(u7));

    b.buf = "i01e";
    try std.testing.expectError(error.InvalidInteger, b.int(u8));

    b.buf = "li123ed1:a1:bee";
    try b.skip();
    try std.testing.expectEqual(b.buf.len, 0);
}


const Torrent = struct {
    piece_length: u64,
    pieces: []const [20]u8,
    files: []File,

    const File = struct {
        path: []const u8,
        length: u64,
    };

    fn readFiles(d: *BenDecoder, files: *std.ArrayList(File)) !void {
        try d.list();
        while (!try d.end()) {
            try d.dict();
            var path = std.ArrayList(u8).init(global.alloc);
            defer path.deinit();
            var length: ?u64 = null;
            while (!try d.end()) {
                const key = try d.str();
                if (std.mem.eql(u8, key, "path")) {
                    try d.list();
                    while (!try d.end()) {
                        try path.append('/');
                        try path.appendSlice(try d.str());
                    }
                } else if (std.mem.eql(u8, key, "length")) length = try d.int(u64)
                else try d.skip();
            }
            if (path.items.len == 0) return error.FileMissingName;
            if (length) |l| {
                try files.append(.{
                    .path = try path.toOwnedSlice(),
                    .length = l
                });
            } else return error.FileMissingLength;
        }
    }

    fn readInfo(d: *BenDecoder, piece_length: *?u64, files: *std.ArrayList(File), pieces: *?[]const [20]u8) !void {
        var name: ?[]const u8 = null;
        var length: ?u64 = null;
        try d.dict();
        while (!try d.end()) {
            const key = try d.str();
            if (std.mem.eql(u8, key, "piece length")) {
                piece_length.* = try d.int(u64);
            } else if (std.mem.eql(u8, key, "length")) {
                length = try d.int(u64);
            } else if (std.mem.eql(u8, key, "name")) {
                name = try d.str();
            } else if (std.mem.eql(u8, key, "pieces")) {
                const data = try d.str();
                if (data.len == 0 or data.len % 20 != 0) return error.InvalidPiecesInTorrent;
                pieces.* = std.mem.bytesAsSlice([20]u8, try global.alloc.dupe(u8, data));
            } else if (std.mem.eql(u8, key, "files")) {
                try readFiles(d, files);
            } else try d.skip();
        }

        const name_ = name orelse return error.TorrentWithoutName;

        // Single-file torrents don't have a file list, so take the top-level "name" and "length" instead.
        if (files.items.len == 0) {
            try files.append(.{
                .path = try global.alloc.dupe(u8, name_),
                .length = length orelse return error.TorrentWithoutLength,
            });
        } else {
            for (files.items) |*f| {
                const new = try std.fmt.allocPrint(global.alloc, "{s}{s}", .{name_, f.path});
                global.alloc.free(f.path);
                f.path = new;
            }
        }
    }

    fn read(filename: [:0]const u8) !Torrent {
        const buf = blk: {
            const file = try std.fs.cwd().openFileZ(filename, .{});
            defer file.close();
            break :blk try file.readToEndAlloc(global.alloc, 100*1024*1024);
        };
        defer global.alloc.free(buf);

        var piece_length: ?u64 = null;
        var pieces: ?[]const [20]u8 = null;
        var files = std.ArrayList(File).init(global.alloc);
        errdefer files.deinit();

        var d = BenDecoder{.buf = buf};
        try d.dict();
        while (!try d.end()) {
            const key = try d.str();
            if (std.mem.eql(u8, key, "info")) {
                try readInfo(&d, &piece_length, &files, &pieces);
            } else {
                try d.skip();
            }
        }

        if (files.items.len == 0) return error.NoFilesInTorrent;

        return Torrent{
            .piece_length = piece_length orelse return error.MissingPieceLength,
            .files = try files.toOwnedSlice(),
            .pieces = pieces orelse return error.NoPiecesInTorrent,
        };
    }

    fn deinit(self: Torrent) void {
        for (self.files) |f| global.alloc.free(f.path);
        global.alloc.free(self.pieces);
        global.alloc.free(self.files);
    }

    fn print(self: Torrent, wr: anytype) !void {
        try wr.print("Torrent info: {} piece(s) of {} byte(s), {} file(s):\n", .{self.pieces.len, self.piece_length, self.files.len});
        for (self.files) |f| try wr.print("  {:12}  {s}\n", .{f.length, f});
        //try wr.print("Pieces ({}):\n", .{self.pieces.len});
        //for (self.pieces) |*p| try wr.print("  {}\n", .{std.fmt.fmtSliceHexLower(p)});
    }
};


const Path = struct {
    dir: std.fs.Dir,
    path: []const u8,
};


const FSFile = struct {
    parent: *const Path, // points into global.paths
    path: []const u8,
    next: ?*FSFile,
    incomplete: bool = false, // set when one of the multi-chunk pieces did not validate

    fn len(file: ?*FSFile) usize {
        var f = file;
        var nfiles: usize = 0;
        while (f) |n| : (f = n.next) nfiles += 1;
        return nfiles;
    }
};

// File size -> array of pointers to insert FSFile entries at
const ScanState = std.AutoHashMap(u64, std.ArrayListUnmanaged(*?*FSFile));

fn scanDir(sizes: *ScanState, path: *const Path, node: *std.Progress.Node) !void {
    var walker = try path.dir.walk(global.alloc);
    defer walker.deinit();
    while (true) {
        const entry = (walker.next() catch |e| {
            std.log.warn("Error reading '{s}/{s}': {}", .{path.path, walker.name_buffer.items, e});
            continue;
        }) orelse break;

        var n = node.start(entry.path, 0);
        n.activate();
        defer n.end();

        switch (entry.kind) {
            .file, .unknown => {},
            else => continue,
        }
        const stat = entry.dir.statFile(entry.basename) catch |e| {
            std.log.warn("Error reading '{s}/{s}': {}", .{path.path, entry.path, e});
            continue;
        };
        if (stat.kind != .file) continue;
        if (sizes.getPtr(stat.size)) |l| {
            for (l.items) |head| {
                const f = try global.alloc.create(FSFile);
                f.* = .{ .parent = path, .path = try global.alloc.dupe(u8, entry.path), .next = head.* };
                head.* = f;
            }
        }
    }
}


fn scanFiles(root_node: *std.Progress.Node) ![]?*FSFile {
    var node = root_node.start("scanning files", 0);
    node.activate();
    defer node.end();

    const files = try global.alloc.alloc(?*FSFile, global.torrent.files.len);
    errdefer global.alloc.free(files);
    var sizes = ScanState.init(global.alloc);
    defer {
        var it = sizes.valueIterator();
        while (it.next()) |v| v.deinit(global.alloc);
        sizes.deinit();
    }
    for (global.torrent.files, files) |t, *f| {
        f.* = null;
        const v = try sizes.getOrPut(t.length);
        if (!v.found_existing) v.value_ptr.* = .{};
        try v.value_ptr.append(global.alloc, f);
    }

    // (FSFile nodes are leaked on error)
    for (global.paths) |*p| try scanDir(&sizes, p, &node);

    for (global.torrent.files, files) |t, f| {
        if (f == null) {
            std.log.warn("No candidates found for '{s}'.", .{t.path});
            unableToComplete();
        }
        // TODO: sorting the list by extension and file name similarity might
        // be a good heuristic to get a matching file faster.
    }

    return files;
}


// Shitty behavior altert: Only advances *ptr when !incomplete.
fn markInvalid(file_idx: usize, ptr: *?*FSFile, incomplete: bool) void {
    const f = ptr.* orelse unreachable;
    if (!f.incomplete)
        std.log.debug("Removing '{s}/{s}' as candidate for '{s}'", .{f.parent.path, f.path, global.torrent.files[file_idx].path});

    if (incomplete) {
        f.incomplete = true;
    } else {
        ptr.* = f.next;
        global.alloc.destroy(f);
    }

    var p = global.candidates[file_idx];
    while (p) |n| : (p = n.next) {
        if (!n.incomplete) return;
    }
    std.log.warn("No more candidates left for '{s}'", .{global.torrent.files[file_idx].path});
    unableToComplete();
}


const Piece = struct {
    chunks: []Chunk,

    const Chunk = struct {
        file: usize, // indexes into torrent.files and global.candidates
        start: u64,
        end: u64,
    };
};


fn allocPieces() ![]Piece {
    const pieces = try global.alloc.alloc(Piece, global.torrent.pieces.len);
    var off: u64 = 0;
    var file: usize = 0;
    for (pieces) |*p| {
        var chunks = std.ArrayList(Piece.Chunk).init(global.alloc);
        var left = global.torrent.piece_length;
        while (left > 0 and file < global.torrent.files.len) {
            const len = global.torrent.files[file].length;
            const chunk = Piece.Chunk{
                .file = file,
                .start = off,
                .end = @min(off + left, len),
            };
            left -= chunk.end - chunk.start;
            if (chunk.end == len) {
                off = 0;
                file += 1;
            } else {
                off += chunk.end - chunk.start;
            }
            if (len > 0) try chunks.append(chunk);
        }
        p.* = .{
            .chunks = try chunks.toOwnedSlice(),
        };
    }
    return pieces;
}


fn readChunk(chunk: *const Piece.Chunk, file: *const FSFile) ![]u8 {
    const buf = try global.alloc.alloc(u8, chunk.end - chunk.start);
    errdefer global.alloc.free(buf);
    const fd = try file.parent.dir.openFile(file.path, .{});
    defer fd.close();
    if (chunk.start > 0) try fd.seekTo(chunk.start);
    if (try fd.readAll(buf) != buf.len) return error.UnexpectedEof;
    return buf;
}


fn hashSinglePiece(node: *std.Progress.Node, piece: usize) !void {
    const chunk = &global.pieces[piece].chunks[0];

    var buf: [32]u8 = undefined;
    var snode = node.start(std.fmt.bufPrint(&buf, "piece #{}", .{piece+1}) catch unreachable, FSFile.len(global.candidates[chunk.file]));
    defer snode.end();
    snode.activate();

    var file = &global.candidates[chunk.file];
    while (file.*) |f| {
        var fnode = snode.start(f.path, 0);
        defer fnode.end();
        fnode.activate();

        const data = readChunk(chunk, f) catch |e| {
            std.log.warn("Unable to read '{s}/{s}': {}", .{f.parent.path, f.path, e});
            markInvalid(chunk.file, file, false);
            continue;
        };
        defer global.alloc.free(data);
        var hash: [20]u8 = undefined;
        std.crypto.hash.Sha1.hash(data, &hash, .{});
        if (!std.mem.eql(u8, &hash, &global.torrent.pieces[piece]))
            markInvalid(chunk.file, file, false)
        else
            file = &f.next;
    }
}


const multi_piece = struct {
    const Data = struct {
        hash: [32]u8, // blake3 hash for speed
        buf: []const u8, // empty string = duplicate or invalid
    };

    const ChunkState = struct {
        // Lookup table to weed out duplicate chunks, to reduce the number of combinations needed.
        hashes: std.AutoHashMap([32]u8, void),
        files: []Data,
        cur: usize = 0, // file currently being hashed
    };

    // leaks memory on error, but only errors on OOM so w/e.
    fn init(node: *std.Progress.Node, piece: usize) ![]ChunkState {
        const chunks = global.pieces[piece].chunks;
        const state = try global.alloc.alloc(ChunkState, chunks.len);
        for (chunks, state) |*c, *cd| {
            cd.* = .{
                .hashes = std.AutoHashMap([32]u8, void).init(global.alloc),
                .files = try global.alloc.alloc(Data, FSFile.len(global.candidates[c.file])),
            };
            var idx: usize = 0;
            var ptr = &global.candidates[c.file];
            while (ptr.*) |f| : ({ ptr = &f.next; idx += 1; }) {
                const cf = &cd.files[idx];
                var rnode = node.start(f.path, 0);
                rnode.activate();
                defer rnode.end();

                cf.* = .{
                    .buf = readChunk(c, f) catch |e| blk: {
                        std.log.warn("Unable to read '{s}/{s}': {}", .{f.parent.path, f.path, e});
                        break :blk "";
                    },
                    .hash = undefined,
                };
                std.crypto.hash.Blake3.hash(cf.buf, &cf.hash, .{});
                if (cd.hashes.get(cf.hash)) |_| {
                    global.alloc.free(cf.buf);
                    cf.buf = "";
                } else try cd.hashes.put(cf.hash, {});
            }
        }
        return state;
    }

    fn deinit(state: []ChunkState) void {
        for (state) |*c| {
            for (c.files) |f| if (f.buf.len > 0) global.alloc.free(f.buf);
            global.alloc.free(c.files);
            c.hashes.deinit();
        }
        global.alloc.free(state);
    }

    fn match(state: []ChunkState, piece: usize) void {
        for (state, 0..) |*c, i| {
            const file_idx = global.pieces[piece].chunks[i].file;
            var j: usize = 0;
            var ptr = &global.candidates[file_idx];
            while (ptr.*) |n| : (j += 1) {
                const f = &c.files[j];
                if (!std.mem.eql(u8, &c.files[c.cur].hash, &f.hash))
                    markInvalid(file_idx, ptr, false)
                else
                    ptr = &n.next;
            }
        }
    }

    fn step(node: *std.Progress.Node, state: []ChunkState, piece: usize, chunk: usize, init_sha1: std.crypto.hash.Sha1) bool {
        for (state[chunk].files, 0..) |*f, i| {
            state[chunk].cur = i;

            var sha1 = init_sha1;
            if (f.buf.len == 0) continue;
            sha1.update(f.buf);
            if (chunk < state.len-1) {
                if (step(node, state, piece, chunk+1, sha1)) return true;
            } else {
                node.completeOne();
                var hash: [20]u8 = undefined;
                sha1.final(&hash);
                if (std.mem.eql(u8, &global.torrent.pieces[piece], &hash)) {
                    match(state, piece);
                    return true;
                }
            }
        }
        return false;
    }

    fn run(node: *std.Progress.Node, piece: usize) !void {
        var num_hashes: usize = 1;
        var num_files: usize = 0;
        for (global.pieces[piece].chunks) |c| {
            const num_candidates = FSFile.len(global.candidates[c.file]);
            num_files += num_candidates;
            num_hashes *|= num_candidates;
        }

        var buf: [32]u8 = undefined;
        var rnode = node.start(std.fmt.bufPrint(&buf, "piece #{}", .{piece+1}) catch unreachable, num_files + num_hashes);
        defer rnode.end();
        rnode.activate();

        const state = try init(&rnode, piece);
        const matched = step(&rnode, state, piece, 0, std.crypto.hash.Sha1.init(.{}));
        deinit(state);

        if (!matched) {
            std.log.warn("No matching combination found for piece {}", .{piece});
            for (global.pieces[piece].chunks) |c| {
                var ptr = &global.candidates[c.file];
                while (ptr.*) |n| : (ptr = &n.next) markInvalid(c.file, ptr, true);
            }
        }
    }
};


fn hashPieces(root_node: *std.Progress.Node) !void {
    // Start hashing pieces with a smaller number of chunks, so that we can
    // weed out non-matching files early on and reduce the number of
    // combinations needed for pieces with more chunks.
    const pieces = try global.alloc.alloc(usize, global.pieces.len);
    for (pieces, 0..) |*p, i| p.* = i;
    std.sort.heap(usize, pieces, {}, struct {
        fn l(i: usize) u64 { return global.pieces[i].chunks.len; }
        fn lt(_: void, a: usize, b: usize) bool { return l(a) < l(b) or (l(a) == l(b) and a < b); }
    }.lt);

    for (pieces) |p| {
        if (global.pieces[p].chunks.len == 1) try hashSinglePiece(root_node, p)
        else try multi_piece.run(root_node, p);
    }
}


fn writePathAsShellArg(out: anytype, str: []const u8) !void {
    try out.writeByte('\'');
    // Prevent a leading '-' from being interpreted as an option
    if (str.len > 0 and str[0] == '-') try out.writeAll("./");
    for (str) |c| switch (c) {
        '\'' => try out.writeAll("'\\''"),
        else => try out.writeByte(c),
    };
    try out.writeByte('\'');
}


fn output_sh(link_cmd: []const u8) !void {
    const stdout = std.io.getStdOut().writer();
    var buf = std.io.bufferedWriter(stdout);
    const wr = buf.writer();
    var pathbuf: [4096]u8 = undefined;

    // Not the most logical order, but this ensures each path is only 'mkdir'ed
    // once and parents are 'mkdir'ed before childs. Not that it matters with
    // -p, but it looks nicer.
    const files = try global.alloc.alloc(usize, global.torrent.files.len);
    defer global.alloc.free(files);
    for (files, 0..) |*f, i| f.* = i;
    std.sort.heap(usize, files, {}, struct {
        fn lt(_: void, a: usize, b: usize) bool {
            const pa = global.torrent.files[a].path;
            const pb = global.torrent.files[b].path;
            const la = std.mem.count(u8, pa, "/");
            const lb = std.mem.count(u8, pb, "/");
            return if (la == lb) std.mem.lessThan(u8, pa, pb) else la < lb;
        }
    }.lt);

    try wr.writeAll("#!/bin/sh\n");
    var last_path: []const u8 = "";
    for (files) |i| {
        const path = std.fs.path.dirnamePosix(global.torrent.files[i].path) orelse "";
        if (!std.mem.eql(u8, path, last_path)) {
            last_path = path;
            try wr.writeAll("mkdir -p ");
            try writePathAsShellArg(wr, path);
            try wr.writeAll("\n");
        }
    }
    for (files) |i| {
        const f = global.torrent.files[i];
        var ptr = global.candidates[i];
        var found = false;
        while (ptr) |cf| : (ptr = cf.next) {
            if (cf.incomplete) continue;
            if (found) try wr.writeAll("#");
            found = true;
            try wr.writeAll(link_cmd);
            try wr.writeAll(" ");
            try writePathAsShellArg(wr, try std.fmt.bufPrint(&pathbuf, "{s}/{s}", .{cf.parent.path, cf.path}));
            try wr.writeAll(" ");
            try writePathAsShellArg(wr, f.path);
            try wr.writeAll("\n");
        }
        if (!found)
            try wr.print("# No candidates found for '{s}'\n", .{ f.path });
    }
    try buf.flush();
}


pub fn main() !void {
    var args = try std.process.ArgIterator.initWithAllocator(global.alloc);
    var filename: ?[:0]const u8 = null;
    var paths = std.ArrayList([:0]const u8).init(global.alloc);
    var output: []const u8 = "cp";
    var silent = false;
    _ = args.next();
    while (args.next()) |arg| {
        if (std.mem.eql(u8, arg, "--help")) usage(true)
        else if (std.mem.eql(u8, arg, "--version")) {
            std.io.getStdOut().writer().writeAll("torreas "++@import("build_options").version++"\n") catch {};
            return;
        } else if (std.mem.eql(u8, arg, "--verbose")) global.log_level = .debug
        else if (std.mem.eql(u8, arg, "--silent")) { global.log_level = .err; silent = true; }
        else if (std.mem.eql(u8, arg, "--partial")) global.partial = true
        else if (std.mem.eql(u8, arg, "--hardlink-sh")) output = "ln"
        else if (std.mem.eql(u8, arg, "--symlink-sh")) output = "ln -s"
        else if (std.mem.eql(u8, arg, "--copy-sh")) output = "cp"
        else if (std.mem.startsWith(u8, arg, "--")) usage(false)
        else if (filename == null) filename = arg
        else try paths.append(arg);
    }
    if (paths.items.len == 0) usage(false);

    global.torrent = if (filename) |n| blk: {
        break :blk Torrent.read(n) catch |e| die("Error reading '{s}': {}", .{ n, e });
    } else usage(false);
    std.log.debug("{} files and {} pieces in torrent.", .{global.torrent.files.len, global.torrent.pieces.len});

    global.paths = try global.alloc.alloc(Path, paths.items.len);
    for (paths.items, global.paths) |p, *g| {
        g.* = Path{
            .dir = std.fs.cwd().openDir(p, .{ .iterate = true }) catch |e| die("Error opening '{s}': {}", .{ p, e }),
            .path = std.fs.cwd().realpathAlloc(global.alloc, p) catch p,
        };
    }

    {
        const root_node = global.progress.start("", 1 + global.torrent.pieces.len);
        if (silent) global.progress.terminal = null; // Is this how the API is supposed to be used?
        defer root_node.end();

        global.candidates = try scanFiles(root_node);
        global.pieces = try allocPieces();
        try hashPieces(root_node);
    }

    output_sh(output) catch {};
}
