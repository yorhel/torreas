<!--
SPDX-FileCopyrightText: Yorhel <projects@yorhel.nl>
SPDX-License-Identifier: MIT
-->

# torreas

Torrent file reassembler.

Requires zig 0.12.

# Build & install

```
zig build --prefix /usr/local --release install
```
